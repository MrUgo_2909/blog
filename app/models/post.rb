class Post < ActiveRecord::Base
has_many :comments, dependent: :destroy
validates_ presence_of :title
validates_ presence_of :body
end
