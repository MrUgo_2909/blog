class Comment < ActiveRecord::Base
belongs_to :post
validates_ presence_of :post_id
validates_ presence_of :body
end
